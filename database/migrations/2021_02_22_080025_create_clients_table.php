<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->index();
            $table->string('name')->nullable();
            $table->date('birth_date')->nullable();
            $table->char('gender')->nullable();
            $table->string('phone')->nullable();
            $table->unsignedBigInteger('nation_id')->index()->nullable();
            $table->integer('status')->default(1);
            $table->string('avatar')->nullable();
            $table->timestamps();
        });
        Schema::table('clients', function (Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('nation_id')->references('id')->on('nations')->onDelete('set null');

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
