<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShipmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hash_code')->unique();
            $table->unsignedBigInteger('order_id')->index();
            $table->unsignedBigInteger('store_id')->index()->nullable();
            $table->text('note')->nullable();
            $table->integer('status')->default(0);
            $table->integer('total_price')->default(0);

            $table->text('store_log')->nullable();
            $table->string('store_name')->nullable();

            $table->timestamps();
        });

        Schema::table('shipments', function (Blueprint $table){
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('set null') ;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipments');
    }
}
