<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('ar_name')->nullable();
            $table->unsignedBigInteger('sub_category_id')->index();
            $table->string('image');
            $table->text('description');
            $table->text('ar_description')->nullable();
            $table->integer('status')->default(0);
            $table->integer('price')->default(0);
            $table->integer('stock')->default(0);
            $table->integer('rate')->default(0);
            $table->integer('handling_days')->default(0);
            $table->integer('visited')->default(0);
            $table->integer('ordered')->default(0);
            $table->string('part_number')->nullable();
            $table->string('brand')->nullable();
            $table->unsignedBigInteger('store_id')->index();
            $table->timestamps();
        });

        Schema::table('products', function (Blueprint $table){
            $table->foreign('store_id')->references('id')->on('stores')->onDelete('cascade');
            $table->foreign('sub_category_id')->references('id')->on('sub_categories')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
