<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
               'email'=>'admin@gmail.com',
                'role'=>User::ADMIN,
               'password'=> bcrypt('wesdwesd'),
            ],
            [
               'email'=>'user@gmail.com',
                'role'=>User::CLIENT,
               'password'=> bcrypt('wesdwesd'),
            ],
            [
                'email'=>'store@gmail.com',
                 'role'=>User::STORE,
                'password'=> bcrypt('wesdwesd'),
             ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
