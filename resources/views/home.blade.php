@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="card-body">
                    @auth
                    The user is authenticated...
                @endauth

                @guest
                     The user is guest...
                @endguest
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
