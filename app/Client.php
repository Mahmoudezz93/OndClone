<?php

namespace App;

use App\Http\Controllers\Traits\ClientTrait;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Client extends Model
{
    //
    use ClientTrait;
    use Sortable;

    const STATUS_VERIFIED = 1;
    const STATUS_NOT_VERIFIED = 2;
    const STATUS_SUSPENDED = 3;

    public $sortable = ['id','age','name' ];
    public $sortableAs = ['reviews_count'];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function nation()
    {
        return $this->belongsTo('App\Nation', 'nation_id');
    }
    public function reviews()
    {
        return $this->hasMany('App\ProductReview', 'client_id');
    }
    public function orders()
    {
        return $this->hasMany('App\Order', 'client_id');
    }
    public function getAvatarAttribute($value)
    {
        if($value)return asset('files/users/images/'.$value);
        return asset('images/not_found.png');

    }
}
