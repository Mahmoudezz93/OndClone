<?php

namespace App\Http\Controllers\Traits;

use App\Address;
use App\Order;
use App\Product;
use App\Shipment;
use App\ShipmentProduct;
use App\User;
use Illuminate\Support\Facades\Hash;

trait OrderTrait
{
    public function place_order($request)
    {
        $order_total                = 0;
        $this->status               = Order::STATUS_REQUESTED;
        $detailed_address           = Address::where('id',$request->address_id)->first();
        $this->address_log          =  'City: '.$detailed_address->city->en_name.' / Address: '.$detailed_address->address ." / Street: ". $detailed_address->street ." / Building No: " . $detailed_address->building . " / Floor: ".$detailed_address->floor ." / Landline : ".$detailed_address->landline." / Landmark : ".$detailed_address->landmark ." / Apartment : " .$detailed_address->apartment ." / Shipping Notes : " .$detailed_address->shipping_note ." .";

        $this->hash_code            =  str_random(12).rand(1,10000);
        $this->client_id            =  $request->user()->client->id;
        $this->delivery_option      =  1;
        $this->status               =  Order::STATUS_REQUESTED;
        $this->address_id           =  $request->address_id;
        $this->total_price          =  $order_total;
        $this->note                 =  $request->note;

        //get address fees with fixed deliver fees
        // $this->delivery_fees        =  10;
        $this->save();

        foreach($request->products as $item){
            $original_product=Product::where('id',$item['id'])->first();
            $shipment =Shipment::where('order_id',$this->id)->where('store_id',$original_product->store->id)->first();
            if(!isset($shipment)){
                $shipment=new Shipment(); 
                
                $shipment->hash_code =str_random(12).rand(1,10000);
                $shipment->order_id  = $this->id;
                $shipment->status    = Shipment::STATUS_REQUESTED;
                $shipment->store_id  = $original_product->store->id;
                $shipment->save();

            }
            $product =new ShipmentProduct();
            $product->product_id            = $original_product->id;
            $product->shipment_id           = $shipment->id;
            $product->product_price         =$original_product->price;
            $product->product_name          =$original_product->name;
            $product->product_log           =$original_product->description;
            $product->qty                   =$item['quantity'] ;
            if($original_product->stock <$item['quantity']){
                return $this->error_response("Cart has item that out of stock ! ");
            }
            $product->save();
            $shipment->total_price=$shipment->total_price +($original_product->price * $item['quantity']);
            $shipment->save();

            $order_total=$order_total+($original_product->price * $item['quantity']);


    }

    $this->total_price = $order_total;
    $this->save();
    }
}