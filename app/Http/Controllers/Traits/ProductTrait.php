<?php

namespace App\Http\Controllers\Traits;

use App\Product;
use App\User;
use Illuminate\Support\Facades\Hash;

trait ProductTrait
{


    public function get_available()
    {
       return self::where('status', Product::STATUS_ONLINE);

    }
    public function is_avaiable(){
        if($this->status==Product::STATUS_ONLINE)
        return 1;
        else 
        return 0;
    }

}
