<?php

namespace App\Http\Controllers\Traits;

use App\Point;
use Illuminate\Support\Facades\Hash;
use App\Http\Misc\Helpers\Base64Handler;

trait ClientTrait
{

    public function saveClientData($request,$user){
        $this->name = $request->name;
        $this->phone = $request->phone;
        $this->user_id =$user->id;
        if($request->has('company_name')){$this->company_name= $request->company_name;}
        if($request->has('avatar')){$this->avatar    = Base64Handler::storeFile($request->avatar, 'users_images');}

        $this->save();

    }

    public function updateClientData($request)
    {
        if($request->has('name'))
            $this->name = $request->name;

        if($request->has('phone'))
            $this->phone = $request->phone;

        if($request->has('gender'))
            $this->gender = $request->gender;

        if($request->has('birth_date'))
            $this->birth_date = $request->birth_date;

        if($request->has('company_name'))
        $this->company_name = $request->company_name;


        if($request->has('avatar')){$this->avatar= Base64Handler::storeFile($request->avatar, 'users_images');}


        $this->save();

        //$this->tags()->sync($request->tags);


    }



}
