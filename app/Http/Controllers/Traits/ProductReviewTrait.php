<?php

namespace App\Http\Controllers\Traits;

use App\Product;
use App\User;
use Illuminate\Support\Facades\Hash;

trait ProductReviewTrait
{
    public function saveReviewData($request,$product)
    {
        $this->rate = $request->rate;
        $this->product_id =$product->id;
        $this->client_id =$request->user()->client->id;

        if($request->has('review'))
            $this->review = $request->review;

        if($request->has('good'))
            $this->good = $request->good;

        if($request->has('bad'))
            $this->bad = $request->bad;
    
        if($request->has('to_recommend'))
            $this->to_recommend = $request->to_recommend;

        $this->save();



    }



}
