<?php

namespace App;

use App\Http\Controllers\Traits\ProductReviewTrait;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    //
    use ProductReviewTrait;

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }


}
