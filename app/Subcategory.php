<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class SubCategory extends Model
{
    //
    use Sortable;
    public function category()
    {
        return $this->belongsTo('App\Category');
    }
    public function products()
    {
        return $this->hasMany('App\Product', 'sub_category_id');
    }
    protected $fillable = ['image','name','category_id'];
    public $sortable = ['id' ,'name'];
    public $sortableAs = ['products_count'];

    public function getImageAttribute($value)
    {
        return asset('files/categories/images/'.$value);
    }

}
