<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSpec extends Model
{
    //

    protected $fillable=["product_id","value","title","type"];
}
