<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function governrate()
    {
        return $this->belongsTo('App\Governrate');
    }
}
