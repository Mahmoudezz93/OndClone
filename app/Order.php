<?php

namespace App;

use App\Http\Controllers\Traits\OrderTrait;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Order extends Model
{
    //
    use Sortable;
    use OrderTrait;
    const STATUS_REQUESTED=1;
    const STATUS_PROCESSING=2;
    const STATUS_SHIPPING=3;
    const STATUS_SHIPPED=4;
    const STATUS_DRAFT=5;
    public $sortable = ['id','hash_code','status','created_at'];
    public $sortableAs = ['shipments_count'];

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id');
    }
    public function address()
    {
        return $this->belongsTo('App\Address', 'address_id');
    }
    public function shipments()
    {
        return $this->hasMany('App\Shipment', 'order_id');
    }



}
