<?php

namespace App\Providers;

use App\Policies\Admin\StorePolicy;
use App\Store;
use App\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Store::class => StorePolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();
        Gate::define('view-super', function ($user) {
            if ($user->role==User::ADMIN){
                return true;
            }else{
                return false;
            }
        });
        Gate::define('view-store', function ($user) {
            $user_with_Store=  User::with('store')->find(Auth::id())->store;
            if ($user->role==User::STORE  ){
                return true;
            }else{
                return false;
            }
        });

        //
    }
}
