<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductAttachment extends Model
{
    //
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function getImageAttribute($value)
    {
        return asset('files/products/files/'.$value);
    }

}
