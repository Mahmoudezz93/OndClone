<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderProducts extends Model
{
    //
    public function original_product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

}
