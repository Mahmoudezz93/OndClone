<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Shipment extends Model
{
    //
    use Sortable;
    const STATUS_REQUESTED=1;
    const STATUS_CONFIRMED=2;
    const STATUS_SHIPPING=3;
    const STATUS_SHIPPED=4;
    const STATUS_DRAFT=5;
    public $sortable = ['id','hash_code','status','created_at'];
    public $sortableAs = ['ordered_products_count'];
    protected $fillable=['hash_code','order_id','status','store_id'];


    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }


    public function store()
    {
        return $this->belongsTo('App\Store', 'store_id');
    }
    public function ordered_products()
    {
        return $this->hasMany('App\ShipmentProduct', 'shipment_id');
    }

}
