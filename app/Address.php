<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //
    protected $fillable = ['first_name', 'last_name','city_id'];
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }



}
