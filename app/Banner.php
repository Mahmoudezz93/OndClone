<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    //
    public function getContentAttribute($value)
    {
        return asset('files/general/images/'.$value);
    }

}
