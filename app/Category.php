<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Category extends Model
{
    //
    use Sortable;
    public $sortable = ['id',
    'name','type'
    ];
    protected $fillable = ['image'];

    public $sortableAs = ['subcategories_count'];


    public function subcategories()
    {
        return $this->hasMany('App\SubCategory', 'category_id');
    }
    public function getImageAttribute($value)
    {
        return asset('files/categories/images/'.$value);
    }


}
