<?php

namespace App;

use App\Http\Controllers\Traits\ProductTrait;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Product extends Model
{
    //
    use ProductTrait;
    use Sortable;

    const STATUS_ONLINE=1;
    const STATUS_INREVIEW=2;
    const STATUS_REJECTED=3;
    const STATUS_OFFLINE=4;
    const STATUS_DRAFT=5;

    public $sortable = ['id','name','sub_category_id','status','rate','ordered','store_id','stock','price','created_at'];
    protected $fillable=['id','name','ar_name','sub_category_id','status','rate','ordered','store_id','stock','price','image','description'];
    public $sortableAs = ['reviews_count'];


    public function reviews()
    {
        return $this->hasMany('App\ProductReview', 'product_id');
    }


    public function specs()
    {
        return $this->hasMany('App\ProductSpec', 'product_id');
    }


    public function getImageAttribute($value)
    {
        if($value)return asset('files/products/images/'.$value);
        return asset('images/not_found.png');
    }


    public function sub_category()
    {
        return $this->belongsTo('App\SubCategory', 'sub_category_id');
    }

    public function images(){
        return $this->hasMany('App\ProductImage','product_id');
    }


    public function attachments(){
        return $this->hasMany('App\ProductAttachment','product_id');
    }

    public function offer()
    {
        return $this->hasOne('App\ProductOffer', 'product_id');
    }

    public function store()
    {
        return $this->belongsTo('App\Store', 'store_id');
    }

}
