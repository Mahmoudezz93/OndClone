<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    //
    public function product()
    {
        return $this->belongsTo('App\Product', 'product_id');
    }

    public function getImageAttribute($value)
    {
        if($value)return asset('files/products/images/'.$value);
        return asset('images/not_found.png');

    }


}
